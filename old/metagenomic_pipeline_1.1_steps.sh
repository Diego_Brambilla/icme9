#!/bin/bash

cat<<EOF
  ______ _ _ _             _             __      ____  __ 
 |  ____(_) | |           (_)            \ \    / /_ |/_ |
 | |__   _| | | ___  _ __  _ _ __   ___   \ \  / / | | | |
 |  __| | | | |/ _ \| '_ \| | '_ \ / _ \   \ \/ /  | | | |
 | |    | | | | (_) | |_) | | |_) |  __/    \  /   | |_| |
 |_|    |_|_|_|\___/| .__/|_| .__/ \___|     \/    |_(_)_|
                    | |     | |                           
                    |_|     |_|                           
EOF




#This is a version of metagenomic_pipeline_1.1.sh but allow to perform
#just a single step.


#==== ARGUMENT CHECK ====
if [ $# -ne 1 ];then
    echo -e "The only argument should be the configuration file \n"
    echo -e "Usage: metagenomic_pipeline_1.1.sh <Confifuration File> \n"
    exit 1
fi

if [ -e $1 ]; then
    echo -e "Sourcing $1 \n"
    source conf.conf
else
    echo "Configuratione file: $1 does not exists!"
    echo -e "Usage: metagenomic_pipeline_1.1.sh <Confifuration File> \n"
    echo "Quit"
    exit 1
fi


#==== Variables sourced from .conf====
echo -e "==============\n"
echo -e "Input folder"
echo $INPUT_DIR
echo -e "==============\n"

echo -e "\nProject name and sample list\n"
echo $PRJ_NAME
echo $SAMPLE_LIST
echo -e "==============\n"

echo -e "Average quality and length\n"
echo $AV_READS_Q
echo $AV_READS_L
echo -e "==============\n"

echo -e "Coassembly parameters\n"
echo $MIN_KMER_LEN
echo $MAX_KMER_LEN
echo $STEP

#====SET OF OUTPUT LOCAL VARIABLES====

wd=$(pwd)

#Filtered reads
export FILT_READS=$wd/$PRJ_NAME/1_FilteredReads

#Assembly
export ASSEMB_READS=$wd/$PRJ_NAME/2_Assembly

#Gene Prediction
export GENE_PRED=$wd/$PRJ_NAME/3_Gene_Prediction

#Mapping
export MAPP=$wd/$PRJ_NAME/4_Mapping

#Coverage
export COV=$wd/$PRJ_NAME/5_Coverages

#taxonomy
export TAX=$wd/$PRJ_NAME/6_Taxonomy


#==== PIPELINE START ====
if [ $FILTERING -ne 0 ];then
    echo "Performing Filtering"
    echo "`date`: filtering "

    echo -e "\nCreating and exporting output folders: filtered\n"
    mkdir -p $wd/$PRJ_NAME/1_FilteredReads

    echo $FILT_READS

    echo "-------------------------------------------------------------------------"
    echo -e "Quality filtering....\n"

    for i in $SAMPLE_LIST; do
	#Number of reads
	echo "Sample $i"
	echo "Reads $i"
	grep "@" $INPUT_DIR/$i.R1.fastq | wc -l

	#===================
	#importazione moduli
	#===================

	sickle pe -q 30 \
	    -l 80 \
	    -f $INPUT_DIR/$i.R1.fastq \
	    -r $INPUT_DIR/$i.R2.fastq \
	    -t sanger \
	    -o $FILT_READS/filtered.$i.R1.fastq \
	    -p $FILT_READS/filtered.$i.R2.fastq \
	    -s $FILT_READS/filtered.single.$i.fastq \
	    -q $AV_READS_Q \
	    -l $AV_READS_L

	echo "Sample $i"
	echo "Filtered reads $i"
	grep "@" $i.R1.fastq | wc -l
    done
    echo "-------------------------------------------------------------------------"
fi


#======================================================================
#Dopo aver trimmato, i file dei singoli campioni vengono uniti per
#progetto.
#======================================================================


if [ $ASSEMBLY -ne 0 ];then
    echo "Performing Assembly"
    
    echo "Create output folder: Assembly"
    mkdir -p $wd/$PRJ_NAME/2_Assembly
    
    echo -e  "Co-assembly....\n"
    echo "Preparing files for Idba_ud "
    
    
    cat $FILT_READS/filtered*R1*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq
    cat $FILT_READS/filtered*R2*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq
    #======================================================================
    #Vanno aggiunte le variabili ambientali dei moduli ($IDBA_HOME etc)
    #======================================================================

    #convertire fastq in fasta
    /usr/local/bioinf/idba-1.1.1/bin/fq2fa \
	--merge $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq\
    $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq \
	$ASSEMB_READS/interleaved.cat.filtered.$PRJ_NAME.fasta

    echo "`date`: Idba started "
    #assembly
    #=====================
    #Implementare per pico
    #=====================

    idba_ud -r $ASSEMB_READS/interleaved.cat.filtered.$PRJ_NAME.fasta \
	-o $ASSEMB_READS/d.$PRJ_NAME.idba \
	--mink $MIN_KMER_LEN \
	--maxk $MAX_KMER_LEN \
	--step $STEP \
	--pre_correction

    echo $ASSEMB_READS
    echo "`date`: Idba Ended"

    echo "-------------------------------------------------------------------------"
fi

if [ $GENE_PREDICTION -ne 0 ];then
    
    echo "Performing Gene Pred"
    
    echo "Create output folder: Gene Prediction"
    mkdir -p $wd/$PRJ_NAME/3_Gene_Prediction
    
    echo "Gene prediction with Prodigal...."

    echo -e "\n `date`: Running Prodigal..."

    #===========================================
    #prodigal -p meta è deprecato (2.X versions)
    #===========================================
    prodigal -i $ASSEMB_READS/d.$PRJ_NAME.idba/contig.fa \
	-d $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta \
	-a $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
	-o $GENE_PRED/prodigal.idba.$PRJ_NAME.gbk \
	-p meta

    echo "\n `date`: Running Annotation with Diamond..."


    diamond blastp --query $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
	--db $INPUT_DIR/uniprot \
	-o $GENE_PRED/diamond.prodigal.idba.$PRJ_NAME.m8 \
	-f 6 --sensitive

    echo -e "`date`: Diamond Ended \n"
    echo "Parsing Diamond output..."

    sort -u -k1,1 $GENE_PRED/diamond.prodigal.idba.$PRJ_NAME.m8 > $GENE_PRED/sort.best.diamond.prodigal.idba.$PRJ_NAME.m8

    paste <(cut -f1 $GENE_PRED/sort.best.diamond.prodigal.idba.$PRJ_NAME.m8) <(cut  -f2 -d '|' $GENE_PRED/sort.best.diamond.prodigal.idba.$PRJ_NAME.m8) > $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab

    z='id_gene\tuniprot_id'
    sed "1i $z" $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab > $GENE_PRED/id_gene2uniprot.$PRJ_NAME.tab

    echo -e  "`date`: Diamond output parsing Ended \n "
    echo "-------------------------------------------------------------------------"
fi

if [ $MAPPING -ne 0 ];then
    echo "Performing Mapping"

    echo "Create output folder: Mapping"
    mkdir -p $wd/$PRJ_NAME/4_Mapping

    echo "Mapping reads against genes.."

    echo "`date`: Building database"

    bowtie2-build $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta $MAPP/bwt.idba.$PRJ_NAME

    echo "`date`: Starting Bowtie2"

    for i in $SAMPLE_LIST; do
	
	bowtie2 -x $MAPP/bwt.idba.$PRJ_NAME \
	    -1 $FILT_READS/filtered.$i.R1.fastq \
	    -2 $FILT_READS/filtered.$i.R2.fastq \
	    -U $FILT_READS/filtered.single.$i.fastq \
	    -S $MAPP/bwt.idba.$i.sam

    done

    echo "`date`: End of Bowtie2"


    echo "-------------------------------------------------------------------------"

fi


if [ $COVERAGE -ne 0 ];then

    echo "Performing Coverage"
    echo "Parsing mapping output..."
    
    echo "Creating output folder: Coverage Calculation"

    mkdir -p $wd/$PRJ_NAME/5_Coverages
    echo "`date`: Calculating gene coverages"

    for i in $SAMPLE_LIST; do
	samtools view -b -o $COV/bwt.idba.$i.bam $MAPP/bwt.idba.$i.sam -S
	samtools sort $COV/bwt.idba.$i.bam $COV/bwt.idba.$i.sort
	bedtools genomecov -ibam $COV/bwt.idba.$i.sort.bam -g $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta > $COV/coverage.$i.prodigal.bt
    
	awk 'BEGIN {pc=""} 
    { 
        c=$1; 
        if (c == pc) { 
            cov=cov+$2*$5; 
        } else { 
            print pc,cov; 
            cov=$2*$5; 
        pc=c}
    } END {print pc,cov}' $COV/coverage.$i.prodigal.bt | tail -n +2 > $COV/coverage.$i.pergene
	
	z="id_gene $i"
	sed "1i $z"  $COV/coverage.$i.pergene > $COV/coverage.$i
    done

    echo "`date`: End of of all"

    echo "-------------------------------------------------------------------------"
fi

if [ $TAXONOMY -ne 0 ];then
    echo "Performing Taxonomy Affiliation"
    echo "Assigning taxonomy to genes with kaiju..."

    echo "Creating output folder: Taxonomy Affiliation"
    mkdir -p $wd/$PRJ_NAME/6_Taxonomy
    echo "`date`: kaiju started"
    kaiju -t $INPUT_DIR/nodes.dmp \
	-f $INPUT_DIR/kaiju_db_nr_euk.fmi \
	-i $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta \
	-o $TAX/taxa.prodigal.idba.$PRJ_NAME.tab

    z="cla\tid_gene\tncbi_tax"
    sed "1i $z"  $TAX/taxa.prodigal.idba.$PRJ_NAME.tab > $TAX/id_gene2tax.tab


    echo "`date`: kaiju ended"
    list=${list// /,coverage.}
    list="coverage.$list"

    python $INPUT_DIR/gencov.py $INPUT_DIR/RNX2PTW2path2ec2uniprt.tab \
	$TAX/id_gene2tax.tab \
	$GENE_PRED/id_gene2uniprot.$PRJ_NAME.tab \
	$list $TAX/final.table.$PRJ_NAME.tab

    ktImportTaxonomy -o $TAX/krona.taxa.$PRJ_NAME.html $TAX/*.tax.krona -m 3
    ktImportText $TAX/*pathway.krona -o $TAX/krona.metacyc.$PRJ_NAME.html

fi
