#!/bin/bash

#Script for analysis of metagenomic data ver 07/02/2017.
#Analisi basate su usearch v8

#======================================================================
#Al posto di dover inserire più volte le stesse cose farei delle flags
#A occhio direi: una flag per il nome degli input (project e lista),
#tipo di dato (pe o se), codifica qualità (30 o 64), kmer minimi e
#massimi, idba step, sì o no sensitive mode in diamond, parametro -f
#di diamond (NB: non mi torna con la documentazione il valore della flag)

#Capire come fare per far girare automaticamente tutta la pipeline.
#Impossibile farlo con un unico job sulle macchine. La prima idea che
#mi viene in mente è costruire uno script bash in cui sono definite le
#flags e da qeusto script viene lanciata una serie di job dipendenti
#uno dall'altro.  Una seconda opzione sarebbe quella di costruire un
#file di configurazione, ad esempio un makefile, in cui sono
#specificati quali passaggi vadano compiuti.
#
#Può valer la pena considerare di gestire anche i file compressi
#======================================================================

#read -p "Insert project name " pr

pr=fillo

#read -p "Insert sample list " list

#======================================================================
#Campire come gestire nel file di configurazione/makefile il fatto che
#in alcuni passaggi sono presenti più elementi (campioni) e in altri
#il nome del progetto
#======================================================================

#======================================================================
#Serve un modo per non arcodare gli input nel codice
#======================================================================
list='C1 C2'

echo "-------------------------------------------------------------------------"
echo "Quality filtering...."
for i in $list; do
#Un po' di interfaccia per l'utente

echo "Sample $i"
echo "Reads $i"
grep "@" $i.R1.fastq | wc -l

#======================================================================
#Per farle andare sulle macchine è necessario aggiungere le
#importazioni dei moduli
#======================================================================

sickle pe -q 30 \
       -l 80 \
       -f $i.R1.fastq \
       -r $i.R2.fastq \
       -t sanger \
       -o filtered.$i.R1.fastq \
       -p filtered.$i.R2.fastq \
       -s filtered.single.$i.fastq

echo "Sample $i"
echo "Filtered reads $i"
grep "@" $i.R1.fastq | wc -l
done

echo "-------------------------------------------------------------------------"

#======================================================================
#Dopo aver trimmato, i file dei singoli campioni vengono uniti per
#progetto.
#======================================================================

echo "Co-assembly...."
echo "Preparing files for Idba_ud "

cat filtered*R1*fastq > cat.filtered.$pr.R1.fastq
cat filtered*R2*fastq > cat.filtered.$pr.R2.fastq
#======================================================================
#Vanno aggiunte le variabili ambientali dei moduli ($IDBA_HOME etc)
#======================================================================

#convertire fastq in fasta
/usr/local/bioinf/idba-1.1.1/bin/fq2fa \
    --merge cat.filtered.$pr.R1.fastq\
    cat.filtered.$pr.R2.fastq \
    interleaved.cat.filtered.$pr.fasta

echo "`date`: Idba started"
#assembly
#======================================================================
#Problema: assembly ha bisogno di moltissima ram: va fatto su PICO.
#Potrebbe essere un limite all'automazione, a meno che non si faccia
#tutto su PICO.
#======================================================================

idba_ud -r interleaved.cat.filtered.$pr.fasta \
	-o ./d.$pr.idba \
	--mink 40 maxk 99 \
	--step 5 \
	--pre_correction

echo "`date`: Idba Ended"

echo "-------------------------------------------------------------------------"
echo "Gene prediction with Prodigal...."

#=======================================================================
#prodigal -p meta è deprecato, era usato nelle versioni 2.X mentre
#nelle versioni 3.X questa opzione è diventata -p anon. C'è una
#ragione particolare per cui dovreste preferire una versione più
#vecchia? Se così non fosse, userei la versione nuova con i comandi
#nuovi.
#=======================================================================
prodigal -i d.$pr.idba/contig.fa \
	 -d prodigal.idba.$pr.fasta \
	 -a prodigal.idba.$pr.faa \
	 -o prodigal.idba.$pr.gbk \
	 -p meta

echo "`date`: Running Annotation with Diamond..."

#-f 6 cosa comporta?
diamond blastp --query prodigal.idba.$pr.faa \
	--db uniprot \
	-o diamond.prodigal.idba.$pr.m8 \
	-f 6 --sensitive

echo "`date`: Diamond Ended"
echo "Parsing Diamond output..."

sort -u -k1,1 diamond.prodigal.idba.$pr.m8 > sort.best.diamond.prodigal.idba.$pr.m8

paste <(cut -f1 sort.best.diamond.prodigal.idba.$pr.m8) <(cut  -f2 -d '|' sort.best.diamond.prodigal.idba.$pr.m8) > geneID2uniprot.$pr.tab

z='id_gene\tuniprot_id'
sed "1i $z" geneID2uniprot.$pr.tab > id_gene2uniprot.$pr.tab

echo "-------------------------------------------------------------------------"

echo "Mapping reads against genes.."

echo "`date`: Building database"

bowtie2-build prodigal.idba.$pr.fasta bwt.idba.$pr

echo "`date`: Starting Bowtie2"
#=======================================================================
#Problema campioni/progetto
#=======================================================================

for i in $list; do
    
    bowtie2 -x bwt.idba.$pr \
	    -1 filtered.$i.R1.fastq \
	    -2 filtered.$i.R2.fastq \
	    -U filtered.single.$i.fastq \
	    -S bwt.idba.$i.sam

done

echo "`date`: End of Bowtie2"


echo "-------------------------------------------------------------------------"

echo "Parsing mapping output..."

echo "`date`: Calculating gene coverages"

#=======================================================================
#Problema campioni/progetto
#=======================================================================

for i in $list; do
    samtools view -b \
	     -o bwt.idba.$i.bam \
	     bwt.idba.$i.sam -S
    samtools sort bwt.idba.$i.bam bwt.idba.$i.sort
    bedtools genomecov -ibam bwt.idba.$i.sort.bam \
	     -g prodigal.idba.$pr.fasta > coverage.$i.prodigal.bt

    awk 'BEGIN {pc=""}{c=$1;if (c == pc) {cov=cov+$2*$5;} else {printpc,cov;cov=$2*$5;pc=c}} END {print pc,cov}'
    coverage.$i.prodigal.bt | tail -n +2 > coverage.$i.pergene

    z="id_gene $i"
    sed "1i $z"  coverage.$i.pergene > coverage.$i
done

echo "`date`: End of all"

echo "-------------------------------------------------------------------------"

echo "Assigning taxonomy to genes with kaiju..."

echo "`date`: kaiju started"

kaiju -t nodes.dmp \
      -f kaiju_db_nr_euk.fmi \
      -i prodigal.idba.$pr.fasta \
      -o taxa.prodigal.idba.$pr.tab

z="cla\tid_gene\tncbi_tax"
sed "1i $z"  taxa.prodigal.idba.$pr.tab > id_gene2tax.tab


echo "`date`: kaiju ended"
list=${list// /,coverage.}
list="coverage.$list"

python gencov.py ptw2rxn2onthology2ec2uniprot.tab \
       id_gene2tax.tab \
       id_gene2uniprot.$pr.tab \
       $list final.table.$pr.tab

ktImportTaxonomy -o krona.taxa.$pr.html *.tax.krona -m 3
ktImportText *pathway.krona -o krona.metacyc.$pr.html
