Connecting to Cineca
====================

To open a ssh connection to the computer cluster MARCONI on CINECA. If you have a Mac or a PC running Linux, start the terminal (black screen icon) and type::

	ssh username@login.marconi.cineca.it


and give your password when prompted. Now you get a welcoming message from CINECA to inform you that you have successfully logged in.

Now follow the tutor instructions and have a look to the MARCONI file system


**Clone software repository**

Clone the scripts on your personal SCRATCH AREA::
	
	cd $CINECA_SCRATCH
	git clone https://gitlab.com/andrea.franzetti/icme9.git

**Install software and dependencies**::

	cd icme9
	./built_deps.sh

**Start an interactive session on a node of your own**

Now you need to start an interactive session and reserve XX cores in a single node on the MARCONI server to run your analyses. One MARCONI node contains XX cores and XXX Gb of RAM.

To start an interactive session type::

	qsub -A train_metage18 -I -l select=1:ncpus=x:mem=xxGB -l walltime=10:00:00 -N icme9_day1 -q XXXXXX -W group_list=train_metage18

	cd $CINECA_SCRATCH/icme9


**Setting variables**

In the file "conf.conf" you can set environment variables and bioinformatic parameters for the analysis. 

Open the file with an editor::

	vim conf_cluster.conf 

and modify it following the instructions of the instructor.

To make the changes active source the file::

	source conf_cluster.conf 
