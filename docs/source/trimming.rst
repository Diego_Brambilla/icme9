Quality trimming
=================

Source the configuration file::

	source conf_cluster.conf

Use Sickle to filter out low quality reads. First create the directory for storing quality-filtered reads::

	mkdir -p $wd/$PRJ_NAME/1_FilteredReads


See the number of raw reads::

	for i in $SAMPLE_LIST; do
   		echo "Sample $i"
   		echo "Reads $i"
   		grep "@" $INPUT_DIR/$i.R1.fastq | wc -l
	done

Have a look to Sickle help::

	sickle pe --help

Now run Sickle on your data::

	for i in $SAMPLE_LIST; do
		sickle pe \
       		-f $INPUT_DIR/$i.R1.fastq \
       		-r $INPUT_DIR/$i.R2.fastq \
       		-t sanger \
      		-o $FILT_READS/filtered.$i.R1.fastq \
       		-p $FILT_READS/filtered.$i.R2.fastq \
       		-s $FILT_READS/filtered.single.$i.fastq \
       		-q $AV_READS_Q \
        	-l $AV_READS_L
	done

See the number of filtered reads::

	for i in $SAMPLE_LIST; do
   		echo "Sample $i"
    		echo "Filtered reads $i"
    		grep "@" $FILT_READS/filtered.$i.R1.fastq | wc -l
	done
