Gene prediction
===============

**Predict genes using Prodigal**

Source the configuration file::

	source conf_cluster.conf

Create a new folder for gene prediction::

	mkdir -p $wd/$PRJ_NAME/3_Gene_Prediction

Run Prodigal::

	prodigal -i $ASSEMB_READS/d.$PRJ_NAME.idba/contig.fa \
	-d $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta \
	-a $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
	-o $GENE_PRED/prodigal.idba.$PRJ_NAME.gbk \
	-p meta

Have a look to the created files::

	cd $GENE_PRED
	ll
