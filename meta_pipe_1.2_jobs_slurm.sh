#!/bin/bash

cat<<"EOF"
    __  __      _         ___   _                      __ ____
   /  |/  /__  / /_____ _/ __ \(_)___  ___     _   __ <  /|__ \
  / /|_/ / _ \/ __/ __ `/ /_/ / / __ \/ _ \   | | / / / / __/ /
 / /  / /  __/ /_/ /_/ / ____/ / /_/ /  __/   | |/ / / / / __/ 
/_/  /_/\___/\__/\__,_/_/   /_/ .___/\___/    |___(_)_(_)____/ 
                             /_/
                               
EOF

printf "\n \033[0;31m THIS IS THE PICO VERSION!\033[0m \n"

#==== ARGUMENT CHECK ====
if [ $# -ne 1 ];then
    echo -e "The only argument should be the configuration file \n"
    echo -e "Usage: metagenomic_pipeline_1.1.sh <Confifuration File> \n"
    exit 1
fi

if [ -e $1 ]; then
    echo -e "Sourcing $1 \n"
    source $1
    echo -e "Configuration file is: $1 \n"
else
    echo "Configuratione file: $1 does not exists!"
    echo -e "Usage: metagenomic_pipeline_1.1.sh <Confifuration File> \n"
    echo "Quit"
    exit 1
fi

#if the assembler is spades, i need odd kmer length
if [ $ASSEMBLER -e 2 ]; then
    IFS=',' read -ra SP_K_VAL <<<"$SPADES_KMER"
    counter=0
    for i in ${SP_K_VAL[*]} ; do
	if [ $((i%2)) -eq 0 ];then
	    echo -e "ERROR"
	    echo -e "$i is a even number: Spades don't accept even numbers."
	    echo -e "Please change the value of kmer length"
	    exit 1
	fi
    done
fi
#==== Variables sourced from .conf====
echo -e "==============\n"
echo -e "Input folder"
echo $INPUT_DIR
echo -e "==============\n"

echo -e "\nProject name and sample list\n"
echo $PRJ_NAME
echo $SAMPLE_LIST
echo -e "==============\n"

echo -e "Average quality and length\n"
echo $AV_READS_Q
echo $AV_READS_L
echo -e "==============\n"

echo -e "Coassembly parameters\n"
echo $MIN_KMER_LEN
echo $MAX_KMER_LEN
echo $STEP
echo $SPADES_KMER

#==== PIPELINE START ====
#set -x

if [ $FILTERING -ne 0 ];then
    ONE=$(sbatch -p bdw_usr_prod --account cin_staff --job-name Filtering --time 1-00:00:00 -N 1 -n 1 --mem=6Gb --export=CONF=$1 steps/1_FILTERING.sh )
    echo $ONE
    A=$(echo $ONE | cut -d" " -f4)
fi

if [ $ASSEMBLY -ne 0 ];then
    if [ $FILTERING -ne 0 ];then
	TWO=$(sbatch -p bdw_usr_prod --account cin_staff --dependency=afterok:$A  --job-name Assembly --time 1-00:00:00 -N 1 -n 36 --mem=100Gb --export=CONF=$1 steps/2_ASSEMBLY.sh)
    else
	TWO=$(sbatch -p bdw_usr_prod --account cin_staff --job-name Assembly --time 1-00:00:00 -N 1 -n 36 --mem=100Gb --export=CONF=$1 steps/2_ASSEMBLY.sh)
    fi
    echo $TWO
    # The following line is needed to extract only the job number
    B=$(echo $TWO | cut -d" " -f4)
fi

if [ $GENE_PREDICTION -ne 0 ];then
    if [ $ASSEMBLY -ne 0 ];then
	THREE=$(sbatch -p bdw_usr_prod --dependency=afterok:$B --account cin_staff --job-name Gene_Pred --time 1-00:00:00 -N 1 -n 36 --mem=100Gb --export=CONF=$1 steps/3_GENE_PREDICTION.sh)
    else
	THREE=$(sbatch -p bdw_usr_prod --account cin_staff --job-name Gene_Pred --time 1-00:00:00 -N 1 -n 36 --mem=100Gb --export=CONF=$1 steps/3_GENE_PREDICTION.sh)
    fi
    echo $THREE
    C=$(echo $THREE | cut -d" " -f4)
fi

if [ $MAPPING -ne 0 ];then
    if [ $GENE_PREDICTION -ne 0 ];then
	FOUR=$(sbatch -p bdw_usr_prod -d afterok:$C --account cin_staff --job-name Mapping --time 1-00:00:00 -N 1 -n 36 --mem=120Gb --export=CONF=$1 steps/4_MAPPING.sh)
    else
	FOUR=$(sbatch -p bdw_usr_prod --account cin_staff --job-name Mapping --time 1-00:00:00 -N 1 -n 36 --mem=120Gb --export=CONF=$1 steps/4_MAPPING.sh)
    fi
    echo $FOUR
    D=$(echo $FOUR | cut -d" " -f4)
fi

if [ $COVERAGE -ne 0 ];then
    if [ $MAPPING -ne 0 ];then
	FIVE=$(sbatch -p bdw_usr_prod -d afterok:$D --account cin_staff --job-name Coverage --time 1-00:00:00 -N 1 -n 36 --mem=120Gb --export=CONF=$1 steps/5_COVERAGE.sh)
    else
	FIVE=$(sbatch -p bdw_usr_prod --account cin_staff --job-name Coverage --time 1-00:00:00 -N 1 -n 36 --mem=120Gb --export=CONF=$1 steps/5_COVERAGE.sh)
    fi
    echo $FIVE
    E=$(echo $FIVE | cut -d" " -f4)
fi

if [ $PARSING -ne 0 ];then
    if [ $COVERAGE -ne 0 ];then
	SIX=$(sbatch -p bdw_usr_prod -d afterok:$E --account cin_staff --job-name Parsing --time 05:00:00 -N 1 -n 1 --mem=6Gb --export=CONF=$1 steps/6_OUTPUT_WRITE.sh)
    else
	SIX=$(sbatch -p bdw_usr_prod --account cin_staff --job-name Parsing --time 05:00:00 -N 1 -n 1 --mem=6Gb --export=CONF=$1 steps/6_OUTPUT_WRITE.sh)
    fi
    echo $SIX
    D=$(echo $SEVEN | cut -d" " -f4)
fi
