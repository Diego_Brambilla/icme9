#!/bin/bash

cd $SLURM_SUBMIT_DIR
source $CONF

echo "Performing Assembly"
echo "Create output folder: Assembly"

mkdir -p $ASSEMB_READS 

echo -e  "Co-assembly....\n"
echo "Preparing files for Idba_ud "

#--get mem and core used directly from pbs
memGB=$(scontrol show jobid -dd $SLURM_JOBID| grep TRES | cut -f2 -d , | cut -f2 -d=)
mem=${memGB%gb}
threads=$(scontrol show jobid -dd $SLURM_JOBID | grep TRES | cut -d= -f3 | cut -d, -f1)
#--end

cat $FILT_READS/filtered*R1*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq
cat $FILT_READS/filtered*R2*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq
cat $FILT_READS/filtered.single*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.single.fastq

case $ASSEMBLER in
    1)
	$LNBIN/idba_bin/fq2fa --merge $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq \
	$ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq \
	$ASSEMB_READS/interleaved.cat.filtered.$PRJ_NAME.fasta

	echo -e "You Chosed to assemly your reads with IDBA_UD"
	echo "`date`: Idba started "
	
	$LNBIN/idba_bin/idba_ud -r $ASSEMB_READS/interleaved.cat.filtered.$PRJ_NAME.fasta \
	    -o $ASSEMB_READS/d.$PRJ_NAME.idba \
	    --mink $MIN_KMER_LEN \
	    --maxk $MAX_KMER_LEN \
	    --step $STEP \
	    --pre_correction \
	    --num_threads $threads
	echo "`date`: Idba Ended"
	echo "-------------------------------------------------------------------------"
	;;

    2)
	echo -e "You Chosed to assemly your reads with meta-Spades"
	echo "`date`: spades started "

	mkdir $ASSEMB_READS/spades_$PRJ_NAME

	#spades needs the value of each -specified: calculating them

	$LNBIN/spades --meta \
	    -1 $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq \
	    -2 $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq \
	    -s $ASSEMB_READS/cat.filtered.$PRJ_NAME.single.fastq \
	    -o $ASSEMB_READS/spades_$PRJ_NAME \
	    -t $threads \
	    -m $mem \
	    -k $SPADES_KMER
    
	
	cp $ASSEMB_READS/spades_$PRJ_NAME/d.$PRJ_NAME.spades.fasta $ASSEMB_READS
    
	echo "`date`: spades Ended"
	echo "------------------------------------------------------"
	;;
    3)
	echo -e "You Chosed to assembly your reads with Megahit"
	echo "`date`: megahit started "

	$LNBIN/megahit \
	    -1 $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq \
	    -2 $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq \
	    -r $ASSEMB_READS/cat.filtered.$PRJ_NAME.single.fastq \
	    -o $ASSEMB_READS/d.$PRJ_NAME.megahit \
	    --k-min 27 \
	    --k-max 99 \
	    --k-step 12 \
	    -t 36 \
	    
	cp -r $ASSEMB_READS/d.$PRJ_NAME.megahit/final.contigs.fa $ASSEMB_READS/d.$PRJ_NAME.megahit/contig.fa 
	
	echo "`date`: megahit Ended"
	echo "------------------------------------------------------"
	;;
esac
